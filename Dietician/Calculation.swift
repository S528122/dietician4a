//
//  Calculation.swift
//  Dietician
//
//  Created by Ekka,Abhinav on 4/12/17.
//  Copyright © 2017 Runyon,Jaque K. All rights reserved.
//

import Foundation

class Calculation
{
    var intakeVal:Double = 0.0
    var workoutVal:Double = 0.0
    func intake(calories:Double)->Double{
        intakeVal = calories
        return intakeVal
    }
    
    func workout(calories:Double)->Double{
    workoutVal = calories
    return workoutVal
    }
}
