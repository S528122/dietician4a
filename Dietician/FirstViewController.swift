//
//  FirstViewController.swift
//  Dietician
//
//  Created by Runyon,Jaque K on 3/5/17.
//  Copyright © 2017 Runyon,Jaque K. All rights reserved.
//

import UIKit
import Parse

class FirstViewController: UIViewController {
   var workout:Double = 0.0
    var calIntake:Double = 0.0
    
    var a:[String] = []
    var b:[String] = []
    var m:[String] = []
    var n:[String] = []
    var x:Double = 0.0
    var y:Double = 0.0
    
    
    var totIN:Double = 0.0
    var totBT:Double = 0.0
    @IBOutlet weak var intakeLBL: UILabel!
    
    
    @IBOutlet weak var workoutLBL: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        intakeLBL.text! = String(totIN)
        
        workoutLBL.text! = String(totBT)
        

           // intakeLBL.text = calIntake
        
           //  workoutLBL.text! = workout

     //  local2 = (UIApplication.shared.delegate as! AppDelegate).global
        // Do any additional setup after loading the view, typically from a nib.
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        intakeLBL.text! = String(totIN)
        
        workoutLBL.text! = String(totBT)
        

        let query1 = PFQuery(className: "CaloriesIntake")
        
        do {
            let results: [PFObject] = try query1.findObjects()
            print(results)
        
        
            for object in results
            {
                if(!a.contains(object["calIntake"] as! String) )
                {
                
                
            self.a.append(object["calIntake"] as! String)
            totIN += Double(object["calIntake"] as! String)!
          
            print(" cal intake is \(object["calIntake"] as! String)")
                                
                }
        }
            
        }
            
        catch
        {
            print(error)
        }
        
        let query2 = PFQuery(className: "CaloriesBurnt")
        
        do {
            let result: [PFObject] = try query2.findObjects()
            print(result)
            for object in result
            {
                
                if(!b.contains(object["calBurnt"] as! String) )
                {

                self.b.append(object["calBurnt"] as! String)
                    
                totBT += Double(object["calBurnt"] as! String)!
                
                print(" cal Burnt is \(object["calBurnt"] as! String)")
                }
                
            }
        }
        catch
        {
            print(error)
        }

                print("Cal Intake array is  \(a) ")
                print("Cal Burnt array is  \(b) ")
        
        
        
        
        intakeLBL.text! = String(totIN)
        
        workoutLBL.text! = String(totBT)
        
        
        self.view.reloadInputViews()
    
    }
  
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        //  intakeLBL.text! = calIntake
        
       //   workoutLBL.text! = workout

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
   
    @IBAction func logoutBTN(_ sender: Any) {
        PFUser.logOut()
    }
     

    @IBAction func unwindToHome(sender: UIStoryboardSegue)
    {
    }

    
    

 }

